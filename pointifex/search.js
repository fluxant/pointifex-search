module.exports = {
    
    /**
      Allows the user to draw a bounding box on the map, and then to search within
      it's confines
     **/
    defineSearchBounds: function(map, options, callback) {
        
        var startPoint = null,
            rectangle = null,
            source = options.url || '';
        
        map['dragging'].disable();
        map['drawableRect'].enable();
        
        map.on('drawrectend', function(options) {
            
            var bounds = options.bounds,
    	        args = [
                    bounds.getSouthWest().lng,
                    bounds.getSouthWest().lat,
                    bounds.getNorthEast().lng,
                    bounds.getNorthEast().lat
                ],
                sourceUrl = source + 'data?bbox=' + args.join(','),
                self = this;
            
            $.ajax({
                url: sourceUrl,
                dataType: 'json',
                success: function(data) {
                    callback(null, data);         
                }
            });
            
            map['dragging'].enable();
            map['drawableRect'].disable();
        });
        
    }
}